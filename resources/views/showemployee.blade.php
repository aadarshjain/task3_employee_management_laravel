@extends('master')
@section('content')
<h1 class="text-center text-info">Employee List</h1>
{{-- Active status model --}}

<div class="modal fade" id="StatusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update status</h5>
        {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
      </div>
      <form action="{{url('update_status_emp')}}" method="POST">
        @csrf
        @method('PUT')

        <p> Are you want to update the status ?</p>
        <input type="hidden" id="updating_id" name="update_emp_id">
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button type="submit" class="btn btn-primary">Yes</button>
      </div>
    </form>
    </div>
  </div>
</div>

{{-- End Active status model --}}

{{-- Delete modal --}}
<div class="modal fade" id="DeleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete record</h5>
        {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
      </div>
      <form action="{{url('delete_emp')}}" method="POST">
        @csrf
        @method('DELETE')

        <p> Are you really want to delete record?</p>
        <input type="hidden" id="deleteing_id" name="delete_emp_id">
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button type="submit" class="btn btn-primary">Yes</button>
      </div>
    </form>
    </div>
  </div>
</div>

{{-- End delete model --}}

{{--  Delete All Selected Modal --}}
<div class="modal fade" id="deleteAllModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Delete all selected records</h5>
          </div>
          <div class="modal-body">
              <p>Are you sure you want to delete all selected records?</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
              <button type="button" class="btn btn-primary" id="confirmDeleteAll">Yes</button>
          </div>
      </div>
  </div>
</div>
{{-- Delete all selected modal --}}

{{-- Create modal --}}
<div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Add Employee</h5>
          </div>
          <div class="modal-body">
              <p>Are you want to add Employee ?</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
              <a href="/" type="button" class="btn btn-primary" id="">Yes</a>
          </div>
      </div>
  </div>
</div>
{{-- Create modal --}}
@if(Session::get('delete_status'))
<div class="alert alert-success alert-dismissible fade show">
  <button type="button" class="close " aria-label="Close" data-bs-dismiss="alert">&times;</button>
    {{Session::get('delete_status')}}
</div>
@endif

@if(Session::get('update_status'))
<div class="alert alert-success alert-dismissible fade show">
  <button type="button" class="close " aria-label="Close" data-bs-dismiss="alert">&times;</button>
    {{Session::get('update_status')}}
</div>
@endif

@if(Session::get('update_record'))
<div class="alert alert-success alert-dismissible fade show">
  <button type="button" class="close " aria-label="Close" data-bs-dismiss="alert">&times;</button>
    {{Session::get('update_record')}}
</div>
@endif


<div class="container">

  <div class="row m-3">
    <div class="col-md-6 ">
      <div class="form-group">
        <form action="/search" method="GET">
          {{-- <form action="{{url('search',1)}}" method="GET"> --}}
        <div class="input-group">
          <input type="text" name="search" class="form-control" placeholder="Search" value="{{ isset($search) ? $search: ''}}">
          <button type="submit" class="btn btn-primary mr-2 ml-2">Search</button>
  
        </div>


      </form>
      </div>
  
    </div>

    <div class="col-md-6">
      <div class="form-group">
    {{-- <form action="{{route('filter',2)}}" method="GET"> --}}
      <form action="/filter" method="GET">
      <div class="input-group">
        <select name="filter" id="filter" class="form-control" >
          <option value="2" @if($filter == 2) selected @endif>Select</option>
          <option value="1" @if($filter == 1) selected @endif>Active</option>
          <option value="0" @if($filter == 0) selected @endif>Not Active</option>
          
        </select>
        <button type="submit" class="btn btn-primary mr-2 ml-2">Filter</button>
      </div>
      
    </form>
      </div>
    </div>

  </div>

  
  
<div >
 
    <button  class="btn btn-danger float-right m-2 btn_delete_all" id="delete_all_selected_record">Delete All Selected</button>
    <button class="btn btn-primary float-right m-2 btn_create" id="btn_create_emp">Create</button>
</div>

  
  <table class="table table-hover table-bordered  m-5">
    <thead class="table-warning">
      <th><input type="checkbox" name="" id="select_all_ids"></th>
      {{-- <th>ID</th> --}}
      <th>Name</th>
      <th>Phone no</th>
      <th>Skills</th>
      <th>Role</th>
      <th>Image</th>
      <th>Experience</th>
      <th>Email</th>
      <th>Status</th>
      <th>Action</th>
    </thead>

    <tbody>
      @foreach ($list as $item)
          <tr id="employee_ids{{$item->id}}">
            <td><input type="checkbox" name="ids" class="checkbox_ids" id="" value="{{$item->id}}"></td>
            {{-- <td>{{$item->id}}</td> --}}
            <td>{{$item->name}}</td>
            <td>{{$item->phone_no}}</td>
            <td>{{$item->skills}}</td>
            <td>{{$item->role}}</td>
            <td class="rounded-circle" width="50" height="50"><img id="image" src="./images/{{ $item->image}}">  </td>
            <td>{{$item->experience}}</td>
            <td>{{$item->email}}</td>
            <td class="text-{{$item->status ? 'success':'danger'}}">{{$item->status ? 'Active':'Not active'}}</td>
            <td>
              <div class="btn-group">
                <a href="edit/{{$item->id}}" class="editbtn btn btn-info  btn-sm mr-2">Edit</a>
                {{-- <a href="delete/{{$item->id}}" class="btn btn-danger mr-2">Delete</a> --}}
                <button type="button" value="{{$item->id}}" class="btn btn-danger btn-sm deletebtn mr-2">Delete</button>
                {{-- <a href="employee_status/{{$item->id}}" class="btn btn-{{$item->status ? 'success':'danger'}}">{{$item->status ? 'Active':'Not active'}}</a> --}}

                <button type="button" value="{{$item->id}}" class="statusbtn btn btn-sm btn-{{$item->status ? 'danger':'success'}}">{{$item->status ? 'Disable':'Enable'}}</button>

              </div>
            </td>
          </tr>
        
      @endforeach
    </tbody>

</table>


{{$list->links()}}

</div>


@endsection

@section('scripts')
<script>
  $(document).ready(function(){

    $('.btn_delete_all').prop('disabled', true);

    $('#btn_create_emp').click(function(e){
    $('#createModal').modal('show');
   });

    $(document).on('click','.statusbtn',function(){
    var empid=$(this).val();
    // alert(empid);
    $('#StatusModal').modal('show');
    $('#updating_id').val(empid);

   });

   $(".checkbox_ids").click(function(){
    $('.btn_delete_all').prop('disabled', false);
   });

   $("#select_all_ids").click(function(){
    $('.btn_delete_all').prop('disabled', false);
   });


   $(function(e){
    $("#select_all_ids").click(function(){
      $('.checkbox_ids').prop('checked',$(this).prop('checked'));

    });


    $('#delete_all_selected_record').click(function(e){

      $('#deleteAllModal').modal('show');
      // e.preventDefault();
      $('#confirmDeleteAll').click(function() {
      var all_ids=[];
      $('input:checkbox[name=ids]:checked').each(function(){
        all_ids.push($(this).val());
      });


      $.ajax({
        url:"{{route('delete_selected_employee')}}",
        type:"DELETE",
        data:{
          ids:all_ids,
          _token:'{{csrf_token()}}',

        },
        success:function(response){
          $.each(all_ids,function(key,val){
            $('#employee_ids'+val).remove();

          });
        }
      });
      $('#deleteAllModal').modal('hide');
    });

    });
   });

   $(document).on('click','.deletebtn',function(){
    var empid=$(this).val();
    // alert(empid);
    $('#DeleteModal').modal('show');
    $('#deleteing_id').val(empid);

   });

  $('#select_all_ids').change(function() {
$('.checkbox_ids').prop('checked', $(this).prop('checked'));
toggleButtons();
});

$('.checkbox_ids').change(function() {
toggleButtons();
});
function toggleButtons() {


$('.checkbox_ids').each(function() {
var row = $(this).closest('tr');
var editButton = row.find('.editbtn');
console.log(editButton);
var deleteButton = row.find('.deletebtn');
var statusButton = row.find('.statusbtn');
var isChecked = $(this).prop('checked');
editButton.attr('disabled', isChecked);

$(".deletebtn").off('click');

deleteButton.prop('disabled', isChecked);
statusButton.prop('disabled', isChecked);
});
}
  });
</script>
@endsection
