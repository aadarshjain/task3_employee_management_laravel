@extends('master')
@section('content')
    <h1 class="text-center text-info">Employee Details</h1>

{{-- Cancel button modal --}}

<div class="modal fade" id="cancelregistrationModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Emploee record</h5>
            </div>
            <div class="modal-body">
                <p>Are you really don't want to add employee?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                <a href="/showEmp" type="button" class="btn btn-primary" id="">Yes</a>
            </div>
        </div>
    </div>
  </div>
  
  {{-- Cancel button modal --}}


    @if(Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show">
        <button type="button" class="close " aria-label="Close" data-bs-dismiss="alert">&times;</button>
        {{Session::get('success')}}
    </div>
    @endif
    <form id ="empform"action="add" method="post" enctype="multipart/form-data">
        @csrf
      <div class="container">
       <div class="d-grid gap-2 d-md-flex justify-content-md-end">
       <a class="btn btn-primary" href="/showEmp" role="button">Show Employee</a>
      
       </div>
        <div class="form-group">
        <label for="name" class="font-weight-bold" >Name</label>
         <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" >
         <span style="color: red">@error('name') {{$message}} @enderror</span>
        </div><br>
        <div class="form-group">
        <label for="phone" class="font-weight-bold" >Phone no.</label>
        <input type="tel" id="phone" name="phone" class="form-control" value="{{ old('phone') }}"   />
        <span style="color: red">@error('phone') {{$message}} @enderror</span>
        
        </div>
        <br>
        <div class="form-group">
        <label for="skills" class="font-weight-bold"  >Skills</label>

        <textarea id="skills"  name="skills" rows="1" cols="20"  class="form-control">{{ old('skills') }}</textarea>
        <span style="color: red">@error('skills') {{$message}} @enderror</span>
        </div>
        <br>
        <div class="form-group">
        <label for="email" class="font-weight-bold">Enter your email:</label>
        <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}">
        <span style="color: red">@error('email') {{$message}} @enderror</span>
        </div>
        <br>
        

        <div class="form-group">
            <label for="roles" class="font-weight-bold">Select Role</label>
            <select name="roles" id="roles" class="form-control" >
                <option value="0">Select Option</option>
               @foreach($roles as $role)
                   <option value="{{ $role }}" @selected(old('roles') == $role)>{{ $role }}</option>
               @endforeach
            </select>
            <span style="color: red">@error('roles') {{$message}} @enderror</span>
        </div>

       <br>

       <div class="form-group">
        <label for="status" class="font-weight-bold">Status</label>
        <label class="switch">
            <input type="checkbox"  id="status" name="status" onclick="$(this).attr('value', this.checked ? 1 : 0)" >
            <span class="slider round"></span>
          </label>
      </div>

       <br>
       <div class="form-group">
        <label for="experience" class="font-weight-bold">Experience(in years)</label>
        <input type="number" name="experience" id="experience"  value="{{ old('experience') }}" class="form-control"><br>
        <span style="color: red">@error('experience') {{$message}} @enderror</span>
       </div>

       <img id="output"  alt="current image">


       <div class="form-group">
        <label for="image" class="font-weight-bold">Image</label>
        {{-- <input type="file" name="image" id="image" class="form-control-file" size="5000000"><br> --}}
        <input type="file" class="form-control" name="image" id="image" onchange="loadFile(event)">
        <span style="color: red">
            @error('image')
                {{ $message }}
            @enderror
        </span>
    </div>

       <input type="submit" value="Submit" class="btn btn-primary">
       <button type="button" class="btn btn-danger m-2" id="btn_cancel" >Cancel</button>
      </div>
    </form>
@endsection

@section('scripts')
<script>
    var loadFile=function(event){
        // var output=document.getElementById('output');
        var output = $('#output')[0];
        output.src=URL.createObjectURL(event.target.files[0]);
    } 
  $(document).ready(function(){
    $('#btn_cancel').click(function(e){
    $('#cancelregistrationModal').modal('show');
   });
  });
  </script>
@endsection

