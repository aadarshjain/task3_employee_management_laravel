<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\EmpRole;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;

class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository){
        $this->employeeRepository = $employeeRepository;
    }

    public function show()
    {
        $roles=$this->employeeRepository->show_role();
        return view('registration', ['roles' => $roles]);
    }

    public function showEmployee(){
      
        
        $filter=2;
        $list=$this->employeeRepository->showEmployee();
        return view('showemployee', compact('list','filter'));
    }

    // public function showEmployee(Request $request){
    //     // dd($request->route('id'));

    //     if($request->route('id')==1){
    //     $search=$request->search;
    //     $filter=1;
    //     $list=$this->employeeRepository->search($search);
    //     return view('showemployee',compact('list','search','filter'));
    //     }
    //     if($request->route('id')==2){
    //         $filter=$request->filter;
    //     $list=$this->employeeRepository->filter($filter);
    //     return view('showemployee',compact('list','filter'));
    //     }
    //     if($request->route('id')==null){
    //         // dd('done');  
    //     $filter=2;
    //     $list=$this->employeeRepository->showEmployee();
    //     return view('showemployee', compact('list','filter'));
    //     }
    // }

    public function add(Request $request,EmployeeRequest $emp_req){
        $status = $request->input('status');
        if($status==null){
            $status=0;
        }else{
            $status=1;
        }
        // dd($request->input('status'));
        $record=$emp_req->validated();

        $data=new Employee;
        if($files=$request->file('image')){
        $name=$files->getClientOriginalName();
        $files->move('images',$name);
        $data->path=$name;
        $data=$name;
        }

$this->employeeRepository->store($record,$data,$request,$status);
            return back()->with('success','data inserted successfully');
    }

    public function edit($id){
        $row=Employee::where('id',$id)->first();
        $roles = EmpRole::pluck('role');
        $data=[
          'Info'=>$row,
          'roles' => $roles ,
          'selectedRole' => $row->role,
        ];
       return view('edit', $data);
    }
    
    public function update(Request $request,EmployeeRequest $emp_req)
    {
        // dd('call');
        $status = $request->input('status');
        if($status==null){
            $status=0;
        }else{
            $status=1;
        }
        
    $record=$emp_req->validated();
       $data=new Employee;
       if($files=$request->file('image')){
       $name=$files->getClientOriginalName();
       $files->move('images',$name);
       $data->path=$name;
       $data=$name;
       }
    $this->employeeRepository->update_record($record,$data,$request,$status);
    return redirect()->route('showEmp')->with('update_record','Record updated successfully');
    }
    public function update_status(Request $request){
    $this->employeeRepository->update_status($request);
    return redirect()->route('showEmp')->with('update_status','Status updated successfully');
    }


    public function search(Request $request){
        $search=$request->search;
        $filter=2;
        $list=$this->employeeRepository->search($search);
        return view('showemployee',compact('list','search','filter'));
    }

    public function filter(Request $request){
        $filter=$request->filter;
        $list=$this->employeeRepository->filter($filter);
        return view('showemployee',compact('list','filter'));
    }

    public function destroy(Request $request){
        $this->employeeRepository->destroy($request);
        return redirect()->back()->with('delete_status','Record deleted successfully');
    }

    public function delete_all_selected(Request $request){
        $this->employeeRepository->delete_all_selected($request);
        return response()->json(["success"=>'Employee have been deleted!']);
    }
}
