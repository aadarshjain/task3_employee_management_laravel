<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpRole extends Model
{
    use HasFactory;
    public $table='employee_management.emp_roles';
}
