<?php
namespace App\Repositories\Interfaces;

Interface EmployeeRepositoryInterface{
  public function show_role();
  public function showEmployee();
  public function store($record,$data,$request,$status);
  public function update_record($record,$data,$request,$status);
  public function update_status($request);
  public function search($search);
  public function filter($filter);
  public function destroy($request);
  public function delete_all_selected($request);
}