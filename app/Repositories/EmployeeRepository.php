<?php
namespace App\Repositories;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Models\Employee;
use App\Models\EmpRole;

class EmployeeRepository implements EmployeeRepositoryInterface{
  public function show_role(){
    return EmpRole::pluck('role');
  }

  public function showEmployee(){
    // if($route==null){
    return Employee::orderBy('name')->paginate(5);
    // }
  }

  public function store($record,$data,$request,$status){

// dd($status);
    $query=new Employee;
$query->name = $request->input('name');
$query->phone_no = $request->input('phone');
$query->skills = $request->input('skills');
$query->role = $request->input('roles');
$query->experience = $request->input('experience');
$query->email = $request->input('email');
$query->status=$status;
$query->image = $data;

$query->save();
  }

  public function update_record($record,$data,$request,$status){
    $update=Employee::find($request->input('empid'));
    $update->name = $request->input('name');
$update->phone_no = $request->input('phone');
$update->skills = $request->input('skills');
$update->role = $request->input('roles');
$update->experience = $request->input('experience');
$update->email = $request->input('email');
$update->image = $data;
$update->status=$status;
$update->save();
  }

  public function update_status($request){
    $emp_id=$request->input('update_emp_id');
    // dd($emp_id);
    $emp=Employee::where('id',$emp_id)->value('status');
  
    if($emp==1){
        Employee::where('id',$emp_id)->update(['status'=>0]);
    }else{
        Employee::where('id',$emp_id)->update(['status'=>1]);
    }
  }

  public function search($search){
    return Employee::where(function($query) use ($search){
      $query->where('name','Ilike',"%$search%");
  })->orderBy('name')->paginate(5)->appends([
    'search' => $search
]);;

  }

  public function filter($filter){
    if($filter==2){
      return Employee::orderBy('name')->paginate(5);
    }else{
    return Employee::where('status',$filter)->orderBy('name')->paginate(5)->appends([
      'filter' => $filter
  ]);
}
  }

  public function destroy($request){
    $emp_id=$request->input('delete_emp_id');
    $emp=Employee::find($emp_id);
    $emp->delete();
  }

  public function delete_all_selected($request){
    $ids=$request->ids;
    Employee::whereIn('id',$ids)->delete();
  }

}