<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $roles = ['Developer', 'Tester', 'Team Lead', 'Manager', 'Senior Developer'];
        $roles = DB::table('employee_management.Emprole')->pluck('role')->toArray();;

        $data = [];

        for ($i = 1; $i <= 20; $i++) {
            $data[] = [
                'name' => 'Employee ' . $i,
                'phone_no' => mt_rand(1000000000, 9999999999),
                'skills' => 'Skill ' . $i,
                'role' => $roles[array_rand($roles)],
                'experience' => mt_rand(1, 10) . ' years',
                'email' => 'employee' . $i . '@example.com',
            ];
        }

        // Insert the array of data into the table
        DB::table('employee_management.employee')->insert($data);
    }
}
