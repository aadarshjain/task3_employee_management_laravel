<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('registration');
// });

Route::get('/',[EmployeeController::class,'show']);
Route::post('add', [EmployeeController::class,'add']);
Route::get('showEmp', [EmployeeController::class,'showEmployee'])->name('showEmp');
Route::get('edit/{id}', [EmployeeController::class,'edit']);
Route::post('update', [EmployeeController::class,'update'])->name('update');
Route::delete('delete_emp', [EmployeeController::class,'destroy']);
// Route::get('employee_status/{id}', [EmployeeController::class,'update_status']);
Route::put('update_status_emp', [EmployeeController::class,'update_status']);
Route::get('search', [EmployeeController::class,'search']);
// Route::get('search/{id}', [EmployeeController::class,'showEmployee'])->name('search');
Route::get('filter', [EmployeeController::class,'filter']);
// Route::get('filter/{id:2}', [EmployeeController::class,'showEmployee'])->name('filter');

Route::delete('/selected-employee',[EmployeeController::class,'delete_all_selected'])->name('delete_selected_employee');
